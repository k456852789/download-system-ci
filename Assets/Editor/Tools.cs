﻿using UnityEditor;
using UnityEngine;

public static class Tools
{
    [MenuItem("Tools/Clear Cache")]
    public static void ClearCache()
    {
        Debug.Log($" Cache Count : { Caching.cacheCount }");

        if (Caching.ClearCache())
        {
            Debug.Log("Successfully cleaned the cache.");
        }
        else
        {
            Debug.Log("Cache is being used.");
        }
    }
}